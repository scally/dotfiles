export PATH="/Applications/Postgres93.app/Contents/MacOS/bin:$PATH"
export PATH="/usr/local/bin:/usr/local/sbin:~/bin:$PATH"
export PATH=$PATH:$HOME/bin

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# make sublime text the default editor
export EDITOR='subl -w'

source /usr/local/etc/bash_completion.d/git-prompt.sh
export PS1='\w$(__git_ps1 " (%s)") 🍀  '

alias bx='bundle exec'
alias fd='foreman start -f Procfile.dev'
alias gc="find . -name '*.orig' -delete"

export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

export ANDROID_SDK_ROOT=/usr/local/opt/android-sdk
export ANDROID_SDK=/usr/local/opt/android-sdk/r17

[ -f ~/.markrc ] && source ~/.markrc
